import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/models/user.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'dart:io' as io;

class DatabaseHelper {
  DatabaseHelper._();

  static final DatabaseHelper instance = new DatabaseHelper._();
  static const _databaseName = 'UserData.db';
  static Database _db;


  Future<Database> get db async{
    if(_db == null){
      return await initDb();
    }
    return _db;
  }


  initDb() async {
    return await openDatabase(join(await getDatabasesPath(), _databaseName),
        version: 1, onCreate: (Database db, int version) async {
          await db.execute(
              "CREATE TABLE User(username TEXT, email TEXT, password TEXT)");
          print("Table is created");
        });


    // io.Directory documentDirectory = await getApplicationDocumentsDirectory();
    // print('db Location' + documentDirectory.path);
    // String path = join(documentDirectory.path, _databaseName);
    // var ourDb = await openDatabase(path, version: 1, onCreate: _onCreate);
    // return ourDb;
  }

  // void _onCreate(Database db, int version) async {
  //   await db.execute("CREATE TABLE User(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, username TEXT, email TEXT, password TEXT)");
  //   print("Table is created");
  // }

  //Insertion
    saveUser(User user) async {
    var dbClient = await db;
    var res = await dbClient.insert("user", user.toJson(),conflictAlgorithm: ConflictAlgorithm.replace);
    var userList = await dbClient.query("user");
    print("userList $userList");
    return res;
  }

  //Deletion
  Future<int> deleteUser(User user) async {
    var dbClient = await db;
    int res = await dbClient.delete("User");
    return res;
  }

  Future<User> getLogin(String email, String password) async {
    var dbClient = await db;
    var res = await dbClient.rawQuery("SELECT * FROM user WHERE email = '$email' and password = '$password'");

    if (res.length > 0) {
      return new User.fromJson(res.first);
    }
    return null;
  }

}