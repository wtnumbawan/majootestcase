import 'package:majootestcase/data/database_helper.dart';
import 'dart:async';

import 'package:majootestcase/models/user.dart';

class LoginRequest {
  Future<User> getLogin(String email, String password) {
    var result = DatabaseHelper.instance.getLogin(email, password);
    return result;
  }
}