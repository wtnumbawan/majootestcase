import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/ui/detail/movie_detail.dart';

class HomeBlocLoadedScreen extends StatelessWidget {
   final List<Data> data;

  const HomeBlocLoadedScreen({Key key, this.data}) : super(key: key);



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
      itemCount: data?.length ?? 0,
      itemBuilder: (context, index) {
        return GestureDetector(
          onTap: (){
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => MovieDetail(data: data[index])));
          },
          child: movieItemWidget(data[index]),
        );
      },
    ),
    );
  }

  Widget movieItemWidget(Data data){
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25.0)
          )
      ),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(25),
            child: Image.network(data.i.imageUrl),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
            child: Text(data.l, textDirection: TextDirection.ltr),
          )
        ],
      ),
    );
  }
}
