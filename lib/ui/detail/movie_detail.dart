import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_response.dart';

class MovieDetail extends StatelessWidget {
  final Data data;
  const MovieDetail({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(data.l, textDirection: TextDirection.ltr),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Image.network(data.i.imageUrl),
            Row(
              children: [
                Container(
                    padding: EdgeInsets.only(left: 10, right: 10),
                    child: Text("Title : ")),
                data.l.isNotEmpty
                    ? Text(data.l, textDirection: TextDirection.ltr)
                    : Text("No Information")
              ],
            ),
            Row(
              children: [
                Container(
                    padding: EdgeInsets.only(left: 10, right: 10),
                    child: Text("Year : ")),
                data.l.isNotEmpty
                    ? Text(data.year.toString())
                    : Text("No Information")
              ],
            ),
            Text("Series"),
            data.series != null
                ? ListView.builder(
                    itemCount: data.series.length,
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    padding: EdgeInsets.only(left: 20, right: 20),
                    itemBuilder: (context, i) {
                      return seriesItemWidget(data.series[i]);
                    })
                : Container(
              child: Text("No Information"),
            )
          ],
        ),
      ),
    );
  }

  Widget seriesItemWidget(Series series) {
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25.0))),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(10),
            child: Image.network(series.i.imageUrl),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
            child: Text(series.l, textDirection: TextDirection.ltr),
          )
        ],
      ),
    );
  }
}
